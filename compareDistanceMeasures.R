#Compare different distance metrics based on time-point data
compareDistances <- function (data1, metricFunctionNames, step=10 ,...) 
{
  #Sort data
  data1 = data1[names(sort(rowMeans(data1))),]
  #Get time-point samples
  splitNames <- strsplit(colnames(data1),".",fixed=TRUE)
  timePoints = NULL
  near = NULL
  
  for (i in 1:length(splitNames)) {
    if (length(splitNames[[i]]) != 3 && !any(grep("stool",splitNames[[i]][2]))) {#This name formating has no time points
	next
      }
      if (any(grep("stool",splitNames[[i]][2]))) {
	  timePoints = c(timePoints,list(c(splitNames[[i]][1],'stool',strsplit(splitNames[[i]][2],'stool')[[1]][2])))
      }else {
	  timePoints = c(timePoints,splitNames[i])
      }
  }
  intermediate = paste(sapply(timePoints, `[`, 1),sapply(timePoints, `[`, 2),sep=".")
  intermediate = unlist(lapply(intermediate,function(x) if (!any(grep('stool',x))) paste(x,'.',sep='') else x))
  timePoints = vector("list",length(intermediate))
  tpCount = 0
  with_timePoints = unique(intermediate)
  for (i in 1:length(with_timePoints)) {
    if ( length(which(intermediate==with_timePoints[i])) > 1) {
      tpCount = tpCount +1
      timePoints[[tpCount]] = colnames(data1)[grep(with_timePoints[i],colnames(data1),fixed=TRUE)]
    }
  }
  
  if (length(timePoints)==0) {
      print("There are no timepoints in this dataset\n")
      return(-1)
  }
  
  #For each distance metric, compute the distance matrix
  for (i in 1:length(metricFunctionNames)) {
    dist = do.call(metricFunctionNames[i],list(as.name("data1"))) 
    d = as.matrix(dist)
    #Get nearest neighbor
    near[[metricFunctionNames[i]]] = c(1,2,3)
    for (k in 1:dim(d)[1]) {
      near[[metricFunctionNames[i]]][k] = rownames(d)[which(d[k,]==min(d[k,-k]))]
    }
  }

  diff_vector = NULL
  timePointClose_vector = NULL
  nrGenus = NULL
  
  #prepare vector because R is being weird
  for (i in 1:length(metricFunctionNames)) {
      diff_vector[[metricFunctionNames[i]]] = c(1,2,3)
      timePointClose_vector[[metricFunctionNames[i]]] = c(1,2,3)
  }
  
  clean = seq(0,dim(data1)[1]-2,step)

  for (kk in 1:length(clean)) {
    p = clean[kk]
    rem = 1:p
    #data = noise.removal(data1, percent=p)
    data = data1[-rem,]
    nrGenus[kk] = dim(data)[1]/dim(data1)[1]
    for (i in 1:length(metricFunctionNames)) {
      
      dist = do.call(metricFunctionNames[i],list(as.name("data"))) 
      d = as.matrix(dist)
      #Get nearest neighbor
      near_sub = NULL
      for (k in 1:dim(d)[1]) {
        r <- rownames(d)[which(d[k,]==min(d[k,-k]))] #There may be more here, and this should be handled somehow!!!
	near_sub[k] = r[1]
      }
      
      z <- (near_sub==near[[metricFunctionNames[i]]])
      diff_vector[[metricFunctionNames[i]]][kk] = length(z[z==TRUE])/length(z)
      
      #Get closest for samples with time-points
      count = 0;
      good = 0;
     
      for (t in 1:tpCount) {
	  for (j in 1:length(timePoints[[t]])) {
	      count = count+1
	      if (near_sub[which(rownames(d)==timePoints[[t]][j])] %in% timePoints[[t]]) {
		good = good + 1
	      }
	  }
      }
      timePointClose_vector[[metricFunctionNames[i]]][kk] = good/count  
    }
    
  }
  removed = NULL
  timePointValues = NULL
  neighborChanges = NULL
  class = NULL
 
  for (i in 1:length(metricFunctionNames)) {
      removed = c(removed,clean)
      timePointValues = c(timePointValues,timePointClose_vector[[metricFunctionNames[i]]])
      neighborChanges = c(neighborChanges,diff_vector[[metricFunctionNames[i]]]) 
      class = c(class,rep(metricFunctionNames[i],length(clean)))
  }
  
  ret = data.frame(removed=removed, timePointValues = timePointValues, neighborValues = neighborChanges, metric = class)
  return(ret)

}